'use strict';

import React from 'react';

class Controls extends React.Component {
    constructor(props) {
        super(props); 
        this.state = {
            message: ''
        }
    }

    onError(message) {
        this.setState({message});
    }

    handleChange(event) {
        const [ file ]  = event.target.files;
        const { name } = event.target;

        /* make sure it's an image */
        if ( !/image/.test(file.type) ) {
            this.onError(`selected '${name}' is not an image file`);
            event.target.value = "";
            return;
        }

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            this.props.update({
                target: name,
                value: reader.result
            })
        }
    }

    render() {
        return (
            <form>
                <h1>Controls</h1>
                <p>
                    {this.state.message || 'select a background and an image to begin'}
                </p>
                <div>
                    <p>Background:</p>
                    <input
                        type="file"
                        name="background"
                        onChange={this.handleChange.bind(this)}
                    />
                </div>
                <div>
                    <p>Image:</p>
                    <input
                        type="file"
                        name="image"
                        onChange={this.handleChange.bind(this)}
                    />
                </div>
            </form>
        );
    }
}

class ActionImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            mode: null,
            prev: null,
            translateX: 0,
            translateY: 0,
            rotate: 0,
            scaleX: 1,
            scaleY: 1
        }


        /**
         * required for easier removal of event listener
         */
        this.handleMouseMove = this.handleMouseMove.bind(this);
    }

    componentDidMount() {
        window.addEventListener(
            'mousemove', 
            this.handleMouseMove
        );
        window.addEventListener(
            'mouseup',
            this.onMouseUp.bind(this)
        );
        this.sendDataList();
    }

    sendDataList() {
        const ignore = ['active', 'mode', 'prev'];
        const datalist = Object.keys(this.state)
            .filter((k) => ignore.indexOf(k) == -1)
            .map((key) => ({ key, value: this.state[key] }));
        this.props.onData(datalist);

    }

    componentWillUnmount() {
        window.removeEventListener(
            'mousemove',
            this.handleMouseMove
        );
    }

    computeDelta(start, current) {
        return {
            x: current.screenX - start.screenX,
            y: current.screenY - start.screenY
        }
    }

    handleMouseMove(event) {

        if ( !this.state.active )
            return;

        const delta = this.computeDelta(this.state.prev, event);

        if ( this.state.mode === 'move' ) {
            this.setState((p) => ({
                translateX: p.translateX + delta.x,
                translateY: p.translateY + delta.y,
                prev: event
            }));
        } else if ( this.state.mode === 'rotate' ) {
            
            const { x, y } = delta;

            /* scale it a little to make it smoother */
            const deg = (-y - x) * 0.6;

            this.setState((p) => ({
                rotate: p.rotate + deg,
                prev: event
            }));
        } else if ( this.state.mode === 'scale' ) {
            let { x, y } = delta;
            x = x / 100;
            y = y / 100;
            this.setState((p) => ({
                prev: event,
                scaleX: p.scaleX + x,
                scaleY: p.scaleY + y
            }));
        }
    }

    onMouseDown(event) {
        event.persist();
        this.setState({
            active: true,
            mode: event.target.dataset.action,
            prev: event
        });
    }

    onMouseUp() {
        this.setState({
            active: false,
            mode: null,
            prev: null
        });
        this.sendDataList();
    }

    render() {

        const style = {
            transform: `
                translate(${this.state.translateX}px, ${this.state.translateY}px)
                rotate(${this.state.rotate}deg)
                scale(${this.state.scaleX}, ${this.state.scaleY})
            `
        }
        return (
            <div style={style} className="action-image">
                <img src={this.props.src}/>
                <div className="image-move" onMouseDown={this.onMouseDown.bind(this)}>
                    <span data-action="move" className="typcn typcn-arrow-move"/>
                </div>
                <div className="image-rotate" onMouseDown={this.onMouseDown.bind(this)}>
                    <span data-action="rotate" className="typcn typcn-arrow-sync"/>
                </div>
                <div className="image-scale" onMouseDown={this.onMouseDown.bind(this)}>
                    <span data-action="scale" className="typcn typcn-eject"/>
                </div>
            </div>
        );
    }
}

class ActionArea extends React.Component {
    render() {
        return ( 
            <div>
                <h1>Action Area</h1>
                <div className="action-area">
                {this.props.image && 
                    <ActionImage onData={this.props.onData} className="action-image" src={this.props.image}/>
                }
                {this.props.background &&
                    <img className="action-background" src={this.props.background}/>
                }
                </div>
            </div>
        );
    }
}

class DataList extends React.Component {
    render() {

        if ( !this.props.data.length ) 
            return null

        return ( 
            <div>
                <h1>Data</h1>
                <table>
                    <tbody>
                    {this.props.data.map((datum) => (
                        <tr key={datum.key}>
                            <td>{datum.key}</td>
                            <td>{datum.value}</td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
        );
    }
}

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            background: null,
            image: null,
            data: []
        }
    }
    update({target, value}) {
        this.setState({
            [target]: value
        });
    }
    recieveDatalist(list) {
        this.setState({
            data: list
        });
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="column column-40">
                        <Controls update={this.update.bind(this)}/>
                        <DataList data={this.state.data}/>
                    </div>
                    <div className="column column-60">
                        <ActionArea 
                            onData={this.recieveDatalist.bind(this)}
                            background={this.state.background}
                            image={this.state.image}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
