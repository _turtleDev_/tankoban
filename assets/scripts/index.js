'use strict';

import ReactDOM from 'react-dom';
import React from 'react';
import App from './app/main.jsx';

ReactDOM.render(
    React.createElement(App),
    document.querySelector('#app')
);


